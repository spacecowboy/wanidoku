package com.nononsenseapps.wanidoku

interface Notifier {
    suspend fun askAbout(text: String, question: String, answers: List<String>)
    suspend fun appendToNotification(answers: List<String>)
}
