package com.nononsenseapps.wanidoku

import com.nononsenseapps.wanikani.client.WaniKaniV2
import com.nononsenseapps.wanikani.client.getAssignmentsFlow
import com.nononsenseapps.wanikani.client.response.SubjectType
import com.nononsenseapps.wanikani.client.response.SubjectVocabulary
import com.nononsenseapps.wanikani.client.response.SubjectVocabularyResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.withContext

suspend fun getRandomVocabulary(
    client: WaniKaniV2,
    config: Config
): SubjectVocabulary = withContext(Dispatchers.IO) {
    val maxLevelGranted = client.getUser().data.subscription.maxLevelGranted

    val levels = if (maxLevelGranted < 60) {
        Array(maxLevelGranted) { index ->
            index + 1
        }
    } else {
        null
    }

    val assignments = client.getAssignmentsFlow(
        passed = config.onlyPassed,
        burned = config.onlyBurned,
        subjectTypes = arrayOf(SubjectType.VOCABULARY),
        levels = levels
    )
        .map { it.data }
        .toList()

    if (assignments.isEmpty()) {
        throw NoVocabException("No ${if (config.onlyBurned == true) "burned" else "passed"} vocabulary to ask about")
    }

    val assignment = assignments.random()
    val subject = when (val x = client.getSubject(assignment.subjectId)) {
        is SubjectVocabularyResponse -> x.data
        else -> error("Got a non-vocabulary subject")
    }

    subject
}

suspend fun timeForConvo(
    config: Config,
    notifier: Notifier,
    client: WaniKaniV2
) {
    val vocab = getRandomVocabulary(
        client = client,
        config = config
    )

    notify(
        notifier = notifier,
        config = config,
        vocab = vocab
    )
}

suspend fun notify(
    notifier: Notifier,
    config: Config,
    vocab: SubjectVocabulary
) {
    val sentence = vocab.contextSentences.random()

    val question = when (config.askAboutVocab) {
        true -> "${vocab.characters}が分かりますか？"
        false -> "分かりますか？"
    }

    val readings = vocab.readings
        .sortedByDescending { it.primary }
        .joinToString("か") {
            "「${it.reading}」"
        }

    notifier.askAbout(
        text = sentence.ja,
        question = question,
        answers = listOf(
            "読み方は${readings}です。",
            sentence.en
        )
    )
}

class NoVocabException(message: String) : Exception(message)
