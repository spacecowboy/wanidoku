package com.nononsenseapps.wanidoku

import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

interface Config {
    val apiKey: String
    val askAboutVocab: Boolean
    val onlyBurned: Boolean?
    val onlyPassed: Boolean?
    val notificationFrequency: Duration
    val onlySyncOnWifi: Boolean
    val dndEnabled: Boolean
    val dndStart: LocalTime
    val dndEnd: LocalTime

    fun isInDnd(): Boolean {
        val (start, end) = nextDndInterval ?: return false
        val now = LocalDateTime.now()

        return now >= start && now < end
    }

    /**
     * Returns the currently active - or next DnD interval.
     * Null if DnD is not enabled.
     */
    val nextDndInterval: Pair<LocalDateTime, LocalDateTime>?
        get() {
            if (!dndEnabled || dndStart == dndEnd) {
                return null
            }

            return if (dndStart < dndEnd) {
                // Start and end both inside the same day
                if (LocalTime.now() >= dndEnd) {
                    tomorrowsDndStart to tomorrowsDndEnd
                } else {
                    todaysDndStart to todaysDndEnd
                }
            } else {
                // Start before 00:00, end after 00:00
                if (LocalTime.now() < dndEnd) {
                    yesterdaysDndStart to todaysDndEnd
                } else {
                    todaysDndStart to tomorrowsDndEnd
                }
            }
        }
}

val Config.todaysDndStart: LocalDateTime
    get() = LocalDateTime.of(LocalDate.now(), dndStart)

val Config.todaysDndEnd: LocalDateTime
    get() = LocalDateTime.of(LocalDate.now(), dndEnd)

val Config.tomorrowsDndStart: LocalDateTime
    get() = todaysDndStart.plus(Duration.ofDays(1))

val Config.tomorrowsDndEnd: LocalDateTime
    get() = todaysDndEnd.plus(Duration.ofDays(1))

val Config.yesterdaysDndStart: LocalDateTime
    get() = todaysDndStart.minus(Duration.ofDays(1))
