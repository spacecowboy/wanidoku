package com.nononsenseapps.wanidoku.android.background

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.nononsenseapps.wanidoku.Notifier
import com.nononsenseapps.wanidoku.android.logError
import org.kodein.di.android.closestDI
import org.kodein.di.instance


const val INPUT_ANSWER = "input_translation"


class NotificationAppendWorker(
    private val appContext: Context,
    private val params: WorkerParameters
) : CoroutineWorker(appContext, params) {
    override suspend fun doWork(): Result {
        return try {
            val answers = params.inputData.getStringArray(INPUT_ANSWER) ?: return Result.failure()

            val di by closestDI(appContext)
            val notifier: Notifier by di.instance()

            notifier.appendToNotification(
                answers = answers.toList()
            )

            Result.success()
        } catch (e: Throwable) {
            logError("Failed to append translation to notification", e)
            Result.failure()
        }
    }
}
