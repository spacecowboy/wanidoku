package com.nononsenseapps.wanidoku.android

import android.content.SharedPreferences
import com.nononsenseapps.wanidoku.Config
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.instance
import java.time.Duration
import java.time.LocalTime

class Preferences(override val di: DI) : DIAware, Config {
    private val sharedPrefs by di.instance<SharedPreferences>()

    override val apiKey: String
        get() = sharedPrefs.getString("api_key", null) ?: ""
    override val askAboutVocab: Boolean
        get() = sharedPrefs.getBoolean("ask_about_vocab", false)
    override val onlyBurned: Boolean?
        get() = when (sharedPrefs.getString("srs_filter", null)) {
            "burned" -> true
            else -> null
        }
    override val onlyPassed: Boolean?
        get() = when (sharedPrefs.getString("srs_filter", null)) {
            "burned" -> null
            else -> true
        }

    override val notificationFrequency: Duration
        get() = Duration.ofMinutes(
            (sharedPrefs.getString("notification_frequency", null) ?: "1440").toLong()
        )
    override val onlySyncOnWifi: Boolean
        get() = sharedPrefs.getBoolean("only_sync_on_wifi", false)
    override val dndEnabled: Boolean
        get() = sharedPrefs.getBoolean("dnd_enabled", false)
    override val dndStart: LocalTime
        get() = LocalTime.ofSecondOfDay(sharedPrefs.getInt("dnd_start", 0).toLong())
    override val dndEnd: LocalTime
        get() = LocalTime.ofSecondOfDay(sharedPrefs.getInt("dnd_end", 0).toLong())
}
