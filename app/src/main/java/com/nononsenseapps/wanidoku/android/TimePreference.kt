package com.nononsenseapps.wanidoku.android

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.util.TypedValue
import androidx.preference.DialogPreference
import androidx.preference.Preference.SummaryProvider
import com.nononsenseapps.wanidoku.R
import java.time.LocalTime

class TimePreference(
    context: Context,
    attrs: AttributeSet?,
    defStyleAttr: Int,
    defStyleRes: Int
) : DialogPreference(context, attrs, defStyleAttr, defStyleRes) {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(
        context,
        attrs,
        getTypedAttr(
            context,
            androidx.preference.R.attr.editTextPreferenceStyle,
            android.R.attr.editTextPreferenceStyle
        )
    )

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0
    )

    init {
        summaryProvider = SummaryProvider<TimePreference> { pref ->
            pref.time.toString()
        }
    }

    var time: LocalTime = LocalTime.MIDNIGHT
        set(value) {
            field = value

            // Save to shared prefs
            persistInt(value.toSecondOfDay())

            // And update summary
            notifyChanged()
        }

    override fun onGetDefaultValue(a: TypedArray, index: Int): Any {
        return a.getInt(index, 0)
    }

    override fun onSetInitialValue(defaultValue: Any?) {
        val secondOfDay = getPersistedInt(defaultValue as? Int ?: 0).toLong()
        time = LocalTime.ofSecondOfDay(secondOfDay)
    }

    override fun getDialogLayoutResource(): Int {
        return R.layout.pref_dialog_time
    }
}

private fun getTypedAttr(
    context: Context,
    attr: Int,
    fallbackAttr: Int
): Int {
    val value = TypedValue()
    context.theme.resolveAttribute(attr, value, true)
    return if (value.resourceId != 0) {
        attr
    } else fallbackAttr
}
