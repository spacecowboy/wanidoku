package com.nononsenseapps.wanidoku.android.background

import androidx.work.*
import com.nononsenseapps.wanidoku.Config
import org.kodein.di.DI
import org.kodein.di.instance
import java.time.Duration
import java.util.concurrent.TimeUnit

fun scheduleNotifications(di: DI, replace: Boolean = false) {
    val workManager: WorkManager by di.instance()
    val config: Config by di.instance()

    val notificationWorkRequest = PeriodicWorkRequestBuilder<NotificationWorker>(
        config.notificationFrequency,
        Duration.ofMinutes(30)
    )
        .setConstraints(
            Constraints.Builder()
                .setRequiredNetworkType(
                    when (config.onlySyncOnWifi) {
                        true -> NetworkType.UNMETERED
                        false -> NetworkType.CONNECTED
                    }
                ).build()
        )
        .setBackoffCriteria(BackoffPolicy.LINEAR, 1, TimeUnit.HOURS)
        .build()

    workManager.enqueueUniquePeriodicWork(
        NotificationWorker.WORK_NAME,
        when (replace) {
            true -> ExistingPeriodicWorkPolicy.REPLACE
            false -> ExistingPeriodicWorkPolicy.KEEP
        },
        notificationWorkRequest
    )
}

fun runImmediateNotification(di: DI) {
    val workManager: WorkManager by di.instance()

    workManager.enqueue(
        OneTimeWorkRequestBuilder<NotificationWorker>()
            .setInputData(
                workDataOf(
                    INPUT_RUN_NOW to true
                )
            )
            .build()
    )
}

fun runImmediateNotificationAppend(di: DI, answers: List<String>) {
    val workManager: WorkManager by di.instance()

    workManager.enqueue(
        OneTimeWorkRequestBuilder<NotificationAppendWorker>()
            .setInputData(
                workDataOf(
                    INPUT_ANSWER to answers.toTypedArray()
                )
            )
            .build()
    )
}
