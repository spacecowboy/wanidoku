package com.nononsenseapps.wanidoku.android

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.getSystemService
import androidx.lifecycle.lifecycleScope
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.nononsenseapps.wanidoku.NoVocabException
import com.nononsenseapps.wanidoku.R
import com.nononsenseapps.wanidoku.android.background.scheduleNotifications
import com.nononsenseapps.wanidoku.timeForConvo
import com.nononsenseapps.wanikani.client.WaniKaniV2
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import org.kodein.di.direct
import org.kodein.di.instance

class SettingsActivity : AppCompatActivity(), DIAware,
    SharedPreferences.OnSharedPreferenceChangeListener {
    override val di by closestDI()

    private val sharedPrefs by instance<SharedPreferences>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()

        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        sharedPrefs.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onResume() {
        super.onResume()
        scheduleNotifications(di, replace = false)
    }


    override fun onDestroy() {
        sharedPrefs.unregisterOnSharedPreferenceChangeListener(this)
        super.onDestroy()
    }

    override fun onSharedPreferenceChanged(prefs: SharedPreferences, key: String) {
        when (key) {
            "api_key" -> trySyncAndExplainErrors()
            "notification_frequency",
            "only_sync_on_wifi" -> scheduleNotifications(di, replace = true)
        }
    }

    private fun trySyncAndExplainErrors() {
        lifecycleScope.launchWhenResumed {
            try {
                val client: WaniKaniV2 by di.instance()
                val username = withContext(Dispatchers.IO) {
                    client.getUser().data.username
                }
                Toast.makeText(
                    this@SettingsActivity,
                    "Successful connection to WaniKani. Hello ${username}.",
                    Toast.LENGTH_SHORT
                ).show()

            } catch (e: Throwable) {
                Toast.makeText(
                    this@SettingsActivity,
                    "Failed to sync with WaniKani. Check your api key: ${e.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            setupBatteryOptimizationPreference()
            setupTestNotificationPreference()
        }

        override fun onDisplayPreferenceDialog(preference: Preference) {
            if (preference is TimePreference) {
                val fragment = TimePreferenceDialogFragmentCompat.newInstance(preference.key)
                fragment.setTargetFragment(this, 0)
                fragment.show(
                    parentFragmentManager,
                    "android.support.v7.preference.PreferenceFragment.DIALOG"
                )
            } else {
                super.onDisplayPreferenceDialog(preference)
            }
        }

        private fun setupTestNotificationPreference() {
            findPreference<Preference>("test_notification")?.let { pref ->
                pref.setOnPreferenceClickListener {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.getting_sentence_please_wait),
                        Toast.LENGTH_SHORT
                    ).show()

                    lifecycleScope.launchWhenCreated {
                        try {
                            val di by closestDI(requireContext())
                            timeForConvo(
                                di.direct.instance(),
                                di.direct.instance(),
                                di.direct.instance()
                            )
                        } catch (e: NoVocabException) {
                            logError("Failed to show notification: ${e.message}", e)
                            Toast.makeText(
                                requireContext(),
                                e.message,
                                Toast.LENGTH_LONG
                            ).show()
                        } catch (e: Throwable) {
                            logError("Failed to show notification: ${e.message}", e)
                            Toast.makeText(
                                requireContext(),
                                "Failed to sync with WaniKani. Check your api key: ${e.message}",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                    true
                }
            }
        }

        private fun setupBatteryOptimizationPreference() {
            findPreference<Preference>("battery_optimization")?.let { pref ->
                pref.setOnPreferenceClickListener {
                    context?.startActivity(
                        Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS)
                    )

                    true
                }
            }
        }

        private fun setBatteryOptimizationSummary() {
            findPreference<Preference>("battery_optimization")?.let { pref ->
                val powerManager = context?.getSystemService<PowerManager>()

                pref.setSummary(
                    when (powerManager?.isIgnoringBatteryOptimizations(context?.packageName)) {
                        true -> R.string.battery_optimization_summary_disabled
                        else -> R.string.battery_optimization_summary_enabled
                    }
                )
            }
        }

        override fun onResume() {
            super.onResume()

            setBatteryOptimizationSummary()
        }
    }
}
