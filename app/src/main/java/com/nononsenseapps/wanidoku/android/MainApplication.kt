package com.nononsenseapps.wanidoku.android

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import androidx.work.WorkManager
import com.nononsenseapps.wanidoku.Config
import com.nononsenseapps.wanikani.client.WaniKaniV2
import com.nononsenseapps.wanikani.client.getWaniKaniClient
import okhttp3.Cache
import okhttp3.OkHttpClient
import org.kodein.di.*
import org.kodein.di.android.x.androidXModule
import java.io.File
import java.util.concurrent.TimeUnit

@Suppress("unused")
class MainApplication : Application(), DIAware {
    override val di by DI.lazy {
        import(androidXModule(this@MainApplication))
        bind<OkHttpClient>() with singleton {
            cachingHttpClient(di.direct.instance<Application>().cacheDir)
        }
        bind<Preferences>() with singleton { Preferences(di) }
        bind<WaniKaniV2>() with provider {
            getWaniKaniClient(
                di.direct.instance<Config>().apiKey,
                okHttpBuilder = di.direct.instance<OkHttpClient>().newBuilder()
            )
        }
        bind<AndroidNotifier>() with singleton { AndroidNotifier(di) }
        bind<SharedPreferences>() with singleton {
            val context = di.direct.instance<Application>()
            PreferenceManager.getDefaultSharedPreferences(context)
        }
        bind<WorkManager>() with provider {
            WorkManager.getInstance(di.direct.instance<Application>())
        }
    }
}

fun cachingHttpClient(
    cacheDirectory: File,
    cacheSize: Long = 10L * 1024L * 1024L,
    connectTimeoutSecs: Long = 30L,
    readTimeoutSecs: Long = 30L
): OkHttpClient {
    val builder: OkHttpClient.Builder = OkHttpClient.Builder()
        .cache(Cache(cacheDirectory, cacheSize))
        .connectTimeout(connectTimeoutSecs, TimeUnit.SECONDS)
        .readTimeout(readTimeoutSecs, TimeUnit.SECONDS)
        .addInterceptor { chain ->
            chain.proceed(
                chain.request().newBuilder()
                    .header("User-Agent", "wanidoku")
                    .build()
            )
        }

    return builder.build()
}
