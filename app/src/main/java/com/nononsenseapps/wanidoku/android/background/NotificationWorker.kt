package com.nononsenseapps.wanidoku.android.background

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.nononsenseapps.wanidoku.Config
import com.nononsenseapps.wanidoku.android.logError
import com.nononsenseapps.wanidoku.android.logInfo
import com.nononsenseapps.wanidoku.timeForConvo
import org.kodein.di.android.closestDI
import org.kodein.di.direct
import org.kodein.di.instance

const val INPUT_RUN_NOW = "RUN_NOW"

class NotificationWorker(
    private val appContext: Context,
    private val params: WorkerParameters
) : CoroutineWorker(appContext, params) {
    override suspend fun doWork(): Result {
        return try {
            val di by closestDI(appContext)

            val runNow = params.inputData.getBoolean(INPUT_RUN_NOW, false)

            val config: Config by di.instance()

            if (!runNow && config.isInDnd()) {
                logInfo("Currently in quiet time - retry later.")
                return Result.retry()
            }

            timeForConvo(
                config = config,
                notifier = di.direct.instance(),
                client = di.direct.instance()
            )
            Result.success()
        } catch (e: Throwable) {
            logError("Failed to show notification", e)
            Result.failure()
        }
    }

    companion object {
        const val WORK_NAME = "com.nononsenseapps.wanidoku.notificationworker"
    }
}
