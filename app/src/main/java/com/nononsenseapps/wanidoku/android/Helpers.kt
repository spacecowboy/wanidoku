package com.nononsenseapps.wanidoku.android

import android.util.Log

fun Any.logError(msg: String, throwable: Throwable? = null) =
    Log.e(javaClass.simpleName, msg, throwable)

fun Any.logInfo(msg: String, throwable: Throwable? = null) =
    Log.i(javaClass.simpleName, msg, throwable)

fun Any.logDebug(msg: String, throwable: Throwable? = null) =
    Log.d(javaClass.simpleName, msg, throwable)
