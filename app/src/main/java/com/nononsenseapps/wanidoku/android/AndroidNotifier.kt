package com.nononsenseapps.wanidoku.android

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.Person
import com.nononsenseapps.wanidoku.Notifier
import com.nononsenseapps.wanidoku.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.instance
import java.time.Instant
import kotlin.random.Random

private const val NOTIFICATION_ID = 353
private const val CHANNEL_ID = "questions"

class AndroidNotifier(override val di: DI) : DIAware, Notifier {
    private val context: Context by instance()

    private val waniDokuPerson = Person.Builder().setName("WaniDoku").build()

    private fun ensureChannelExists() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = context.getString(R.string.notification_channel_name)
            val descriptionText = context.getString(R.string.notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
                enableVibration(false)

            }
            NotificationManagerCompat.from(context)
                .createNotificationChannel(channel)
        }

    }

    override suspend fun askAbout(
        text: String,
        question: String,
        answers: List<String>
    ) {
        val timestamp = Instant.now().toEpochMilli()

        val answerI = Intent(context, ScheduleReceiver::class.java).apply {
            action = ACTION_TRANSLATE
            data = Uri.parse("${Random.nextLong()}")
            putStringArrayListExtra(EXTRA_ANSWER, ArrayList(answers))
        }
        val answerPI = PendingIntent.getBroadcast(context, 0, answerI, PendingIntent.FLAG_IMMUTABLE)

        notify(
            listOf(
                NotificationCompat.MessagingStyle.Message(text, timestamp, waniDokuPerson),
                NotificationCompat.MessagingStyle.Message(
                    question,
                    timestamp,
                    waniDokuPerson
                )
            ),
            answerIntent = answerPI
        )
    }

    private suspend fun notify(
        messages: Iterable<NotificationCompat.MessagingStyle.Message>,
        answerIntent: PendingIntent?
    ) = withContext(Dispatchers.Main) {
        val anotherI = Intent(context, ScheduleReceiver::class.java).apply {
            action = ACTION_ANOTHER
        }
        val anotherPI = PendingIntent.getBroadcast(context, 0, anotherI, PendingIntent.FLAG_IMMUTABLE)

        val style = NotificationCompat.MessagingStyle(
            waniDokuPerson
        )

        for (message in messages) {
            style.addMessage(message)
        }

        val notification = NotificationCompat.Builder(context, CHANNEL_ID)
            .setStyle(style)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .let {
                if (answerIntent != null) {
                    it.addAction(R.drawable.ic_translate_24, "Translate", answerIntent)
                } else {
                    it.addAction(R.drawable.ic_refresh_24, "Another", anotherPI)
                }
            }
            .build()

        ensureChannelExists()

        NotificationManagerCompat.from(context)
            .notify(
                NOTIFICATION_ID,
                notification
            )
    }

    override suspend fun appendToNotification(answers: List<String>) {
        val notification =
            (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
                .activeNotifications
                .find { it.id == NOTIFICATION_ID }
                ?.notification
                ?: return

        val style =
            NotificationCompat.MessagingStyle.extractMessagingStyleFromNotification(notification)
                ?: return

        val timestamp = Instant.now().toEpochMilli()

        notify(
            style.messages + answers.map { answer ->
                NotificationCompat.MessagingStyle.Message(
                    answer,
                    timestamp,
                    waniDokuPerson
                )
            },
            answerIntent = null
        )
    }
}
