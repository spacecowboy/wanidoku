package com.nononsenseapps.wanidoku.android

import android.text.format.DateFormat.is24HourFormat
import android.view.View
import android.widget.TimePicker
import androidx.core.os.bundleOf
import androidx.preference.PreferenceDialogFragmentCompat
import com.nononsenseapps.wanidoku.R
import java.time.LocalTime

class TimePreferenceDialogFragmentCompat : PreferenceDialogFragmentCompat() {
    private lateinit var timePicker: TimePicker

    override fun onDialogClosed(positiveResult: Boolean) {
        if (positiveResult) {
            val pref = preference
            if (pref is TimePreference) {
                val newTime = LocalTime.of(timePicker.hour, timePicker.minute)
                if (pref.callChangeListener(newTime.toSecondOfDay())) {
                    pref.time = newTime
                }
            }
        }
    }

    override fun onBindDialogView(view: View) {
        super.onBindDialogView(view)

        timePicker = view.findViewById(R.id.edit)
            ?: error("Missing timepicker view in dialog layout")

        val pref = preference

        val value = if (pref is TimePreference) {
            pref.time
        } else {
            LocalTime.MIDNIGHT
        }

        timePicker.setIs24HourView(is24HourFormat(context))
        timePicker.hour = value.hour
        timePicker.minute = value.minute
    }

    companion object {
        fun newInstance(key: String) = TimePreferenceDialogFragmentCompat().apply {
            arguments = bundleOf(
                ARG_KEY to key
            )
        }
    }
}
