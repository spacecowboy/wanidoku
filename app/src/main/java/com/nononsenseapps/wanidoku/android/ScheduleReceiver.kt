package com.nononsenseapps.wanidoku.android

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.nononsenseapps.wanidoku.android.background.runImmediateNotification
import com.nononsenseapps.wanidoku.android.background.runImmediateNotificationAppend
import org.kodein.di.android.closestDI

const val ACTION_TRANSLATE = "com.nononsenseapps.wanidoku.translate"
const val ACTION_ANOTHER = "com.nononsenseapps.wanidoku.another"
const val EXTRA_ANSWER = "com.nononsenseapps.wanidoku.answer"

class ScheduleReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val di by closestDI(context)

        when (intent.action) {
            ACTION_TRANSLATE -> {
                intent.getStringArrayListExtra(EXTRA_ANSWER)?.let { answers ->
                    runImmediateNotificationAppend(di, answers)
                }
            }
            ACTION_ANOTHER -> {
                runImmediateNotification(di)
            }
            else -> {
            }
        }
    }
}
