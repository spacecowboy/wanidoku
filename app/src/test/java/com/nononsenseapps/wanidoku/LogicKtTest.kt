package com.nononsenseapps.wanidoku

import org.junit.Assert.*
import org.junit.Test

class LogicKtTest {
    @Test
    fun boolsSortFalseToTrue() {
        val x = listOf(false, true, false)

        assertEquals(listOf(true, false, false), x.sortedByDescending { it })
    }
}
