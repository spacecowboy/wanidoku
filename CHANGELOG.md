# 1.0.6
Jonas Kalderstam (2):
  * [3d5915a] Fixed crash for new vocabulary using webm audio format

# 1.0.5
Jonas Kalderstam (2):
  * [b2f5dcc] Updated to latest wanikani-client version
  * [6307f9b] Added a better error message when there is no vocab to ask
         about

Poussinou (1):
  * [ffc6b80] Add README.md

# 1.0.4
Jonas Kalderstam (3):
  * [72dab3b] Added readings to notification

# 1.0.3
Jonas Kalderstam (3):
  * [c597471] Enabled minification of app
  * [4539def] Improved background tasks

# 1.0.2
Jonas Kalderstam (3):
  * [6fe19b5] Update to latest android gradle plugin to fix R8 issues

# 1.0.1
Jonas Kalderstam (1):
  * [5107a15] Fixed runtime error due to R8 minification

# 1.0.0

First release!
