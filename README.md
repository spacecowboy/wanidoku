WaniDoku
--------

WaniDoku notifies you about a WaniKani context sentence at your desired pace - allowing you to be exposed to even more Japanese regularly throughout your day.

To ensure notifications are actually displayed remember to disable battery optimization for the app.

You need to be a WaniKani subscriber to use this app.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.nononsenseapps.wanidoku/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=com.nononsenseapps.wanidoku)