# 1.0.5
Jonas Kalderstam (2):
  * [b2f5dcc] Updated to latest wanikani-client version
  * [6307f9b] Added a better error message when there is no vocab to ask
         about

Poussinou (1):
  * [ffc6b80] Add README.md
